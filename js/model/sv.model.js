function SinhVien(id, name, email, password, math, physics, chemistry) {
  this.id = id;
  this.name = name;
  this.email = email;
  this.password = password;
  this.math = math;
  this.physics = physics;
  this.chemistry = chemistry;
  this.average = (this.math + this.physics + this.chemistry) / 3;
}
