const BASE_URL = "https://62db6ca3d1d97b9e0c4f336e.mockapi.io";

function getDSSV() {
  onLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      console.log(res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}

function themSV() {
  onLoading();
  let newSV = getInfoFromForm();

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      offLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function resetInfoSV() {
  return resetInput();
}

function suaSinhVien(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      console.log(res);
      let sv = res.data;

      showInfoToForm(sv);
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}

function updateInfoSV() {
  onLoading();
  let newSV = getInfoFromForm();

  axios({
    url: `${BASE_URL}/sv/${newSV.id}`,
    method: "PUT",
    data: newSV,
  })
    .then(function (res) {
      offLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}

getDSSV();

function onLoading() {
  document.getElementById("loading").style.display = "flex";
}

function offLoading() {
  document.getElementById("loading").style.display = "none";
}
