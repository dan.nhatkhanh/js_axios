getInfoFromForm = function () {
  const id = document.getElementById(`txtMaSV`).value;
  const name = document.getElementById(`txtTenSV`).value;
  const email = document.getElementById(`txtEmail`).value;
  const password = document.getElementById(`txtPass`).value;
  const math = document.getElementById(`txtDiemToan`).value * 1;
  const physics = document.getElementById(`txtDiemLy`).value * 1;
  const chemistry = document.getElementById(`txtDiemHoa`).value * 1;

  let sv = new SinhVien(id, name, email, password, math, physics, chemistry);

  return sv;
};

renderDSSV = function (dssv) {
  var contentHTML = "";

  dssv.forEach((sv) => {
    var contentTr = `<tr>
        <td>${sv.id}</td>
        <td>${sv.name}</td>
        <td>${sv.email}</td>
        <td>${sv.average}</td>
        <td>
        <button onclick="xoaSinhVien('${sv.id}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaSinhVien('${sv.id}')" class="btn btn-warning">Sửa</button>
        </td>
        </tr>`;

    contentHTML += contentTr;
  });

  console.log("contentHTML: ", contentHTML);

  document.getElementById(`tbodySinhVien`).innerHTML = contentHTML;
};

resetInput = function () {
  document.getElementById(`txtMaSV`).value = "";
  document.getElementById(`txtTenSV`).value = "";
  document.getElementById(`txtEmail`).value = "";
  document.getElementById(`txtPass`).value = "";
  document.getElementById(`txtDiemToan`).value = "";
  document.getElementById(`txtDiemLy`).value = "";
  document.getElementById(`txtDiemHoa`).value = "";
};

function showInfoToForm(sv) {
  document.getElementById(`txtMaSV`).value = sv.id;
  document.getElementById(`txtTenSV`).value = sv.name;
  document.getElementById(`txtEmail`).value = sv.email;
  document.getElementById(`txtPass`).value = sv.password;
  document.getElementById(`txtDiemToan`).value = sv.math;
  document.getElementById(`txtDiemLy`).value = sv.physics;
  document.getElementById(`txtDiemHoa`).value = sv.chemistry;
}
